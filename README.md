# Camera

Take pictures and videos on your computer, tablet, or phone


## About

This is a simple camera app designed for Phosh or GNOME. It is designed to work well on mobile Linux devices (such as the PinePhone or Librem 5) but it will run on any Linux computer with a connected camera.


## Camera vs. Cheese

[Cheese](https://gitlab.gnome.org/GNOME/cheese) is an existing camera app. Why a new app, rather than making Cheese adaptive? Cheese is intended as a photo booth app--it has special effects and filters, a "burst" mode, and such. Camera is more like the app on your phone--it's for basic photo and video capture, with a simple, adaptive interface.


## Building

The recommended way to build an run Camera is using [GNOME Builder](https://flathub.org/apps/details/org.gnome.Builder). Just clone the repository and click Run. Builder will make sure all dependencies are downloaded.

If you want/need to build Camera manually, the following commands should do it:

    meson _build
    ninja -C _build
    
Then to run Camera:

    _build/src/camera

You will need [libhandy](https://gitlab.gnome.org/GNOME/libhandy) and [libaperture](https://gitlab.gnome.org/jwestman/libaperture) installed.


## Packaging

Camera has very little branding; it is intended to be a generic camera app. However, this doesn't always work well with distributions, which need to avoid conflicting package names.

To rename the executable file, use the `-Dexecutable_name=<package-name>` argument when running Meson. The recommended package name is "pinhole". This will not change any user-visible strings, only the name of the executable file.


## Contributing

Contributions are welcome! If you find a bug or would like to request a feature, please
[file an issue](https://gitlab.gnome.org/jwestman/camera/-/issues/new).

As Camera is hosted on GNOME infrastructure, all contributors are expected to
follow [GNOME's Code of Conduct](https://wiki.gnome.org/Foundation/CodeOfConduct).


## Hacking

For an overview of the codebase, see [HACKING.md](HACKING.md).


## License

Copyright 2020 James Westman <james@flyingpimonster.net>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
